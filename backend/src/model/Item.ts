import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;

import { productSchema } from './Product';
import { storageSchema } from './Storage';

const itemSchema = new Schema({
  product: {
    type: productSchema,
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  expirationDate: {
    type: Date,
  },
  manufactureDate: {
    type: Date
  },
  storage: {
    type: storageSchema
  }
});

export const Item = mongoose.model('Item', itemSchema);
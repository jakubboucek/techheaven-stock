import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;

export const identifierSchema = new Schema({
  value: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  }
})

export const Identifier = mongoose.model('Identifier', identifierSchema);
import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;
import { identifierSchema } from './Identifier';
import { storageSchema } from './Storage';

export const productSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  identifier: {
    type: [identifierSchema],
  },
  buyPrice: {
    type: Number
  },
  sellPrice: {
    type: Number
  },
  description: {
    type: String
  },
  storedIn: {
    type: storageSchema
  },
  itemsInPack: {
    type: Number
  },
  unit: {
    type: String
  }
});

export const Product = mongoose.model('Product', productSchema);
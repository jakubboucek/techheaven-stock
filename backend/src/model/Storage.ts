import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;

export const storageSchema = new Schema({
  name: {
    type: String,
    required: true,
  }
});

export const Storage = mongoose.model('Storage', storageSchema);
import { getDB } from '../../utils/connectDB';
import { Product } from '../../model/Product';

export const createProduct = async (req, res) => {
  try {
    await getDB();

    const queryKeys = Object.keys(req.query);

    const productParams = {};

    for (const i in queryKeys) {
      if (queryKeys[i] === 'name') {
        req.query[queryKeys[i]] = req.query[queryKeys[i]].toLowerCase();
      }
      productParams[queryKeys[i]] = req.query[queryKeys[i]];
    }

    const newProduct = new Product(productParams);
    await newProduct.save();
    res.json({

    })
    return;
  } catch (err) {
    res.json({
      error: err,
    })
  }
}
import * as mongoose from 'mongoose';

import { getDB } from '../../utils/connectDB';
import { Product } from '../../model/Product';

export const deleteProduct = async (req, res) => {
  try {
    await getDB();

    if (req.query.id) {
      await Product.remove({ _id: mongoose.Types.ObjectId(req.query.id) });
      res.json({});
      return;
    } else {
      res.json({
        error: 'You need to provide id of the product you want to delete'
      })
      return;
    }
  } catch (error) {
    res.json({ error });
  }
}
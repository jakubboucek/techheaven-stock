import { getDB } from '../../utils/connectDB';

import { Product } from '../../model/Product';
import { Item } from '../../model/Item';

export const findProduct = async (req, res) => {
  try {
    await getDB();

    let identifier;
    if (req.query.identifier) {
      identifier = JSON.parse(req.query.identifier);
      const product = await Product.find({ identifier: { $in: identifier } });
      res.json({
        result: product
      })
      return;
    } else {
      const allProducts = await Product.find({}).exec();

      const allProductsWithItems = [];

      for (const i in allProducts) {
        allProductsWithItems.push({
          ...allProducts[i]._doc,
          items: await Item.find({ "product.name": allProducts[i].name }),
        })
      }

      res.json({
        result: allProductsWithItems
      })
      return;
    }

  } catch (err) {
    res.json({
      error: err
    })
  }
}
import { getDB } from '../../utils/connectDB';
import { Item } from '../../model/Item';
import { Product } from '../../model/Product';

export const addItem = async (req, res) => {
  try {
    await getDB();

    if (req.query.productName && req.query.quantity) {
      const product = await Product.findOne({ name: req.query.productName });

      const newItem = new Item({ product: product, quantity: req.query.quantity });

      await newItem.save();

      res.json({});
    }
  } catch (error) {
    res.json({ error });
  }
}
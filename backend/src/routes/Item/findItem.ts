import { getDB } from '../../utils/connectDB';
import { Item } from '../../model/Item';

export const findItem = async (req, res) => {
  try {
    await getDB();

    if (req.query.id) {
      res.json({
        result: await Item.find({ _id: req.query.id }),
      })
      return;
    } else {
      res.json({
        result: await Item.find({}),
      })
      return;
    }
  } catch (error) {
    res.json({ error });
  }
}
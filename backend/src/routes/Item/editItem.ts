import { getDB } from '../../utils/connectDB';
import { Item } from '../../model/Item';

export const editItem = async (req, res) => {
  try {
    await getDB();

    const queryKeys = Object.keys(req.query);

    if (queryKeys.length > 0 && req.query.id) {
      const options = {};

      for (const i in queryKeys) {
        options[queryKeys[i]] = req.query[queryKeys[i]]
      }

      await Item.updateOne({ _id: req.query.id }, options);
      res.json({});
      return;
    } else {
      res.json({
        error: 'You need to provide ID of the item and the properties to edit'
      })
      return;
    }

  } catch (error) {
    res.json({ error })
  }
}
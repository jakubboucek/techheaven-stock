import * as mongoose from 'mongoose';

import { getDB } from '../../utils/connectDB';
import { Item } from '../../model/Item';

export const removeItem = async (req, res) => {
  try {
    await getDB();

    if (req.query.id) {
      await Item.remove({ _id: mongoose.types.ObjectId(req.query.id) })
      res.json({});
      return;
    } else {
      res.json({
        error: 'You need to provide ID of the item to delete'
      })
      return;
    }
  } catch (error) {
    res.json({ error })
  }
}
import { getDB } from '../../utils/connectDB';
import { Storage } from '../../model/Storage';

export const createStorage = async (req, res) => {
  try {
    await getDB();

    if (req.query.name) {
      const newStorage = new Storage({ name: req.query.name });
      await newStorage.save();
      res.json({});
      return;
    }
  } catch (error) {
    res.json({ error });
  }
}
import {getDB} from '../../utils/connectDB';
import {Storage} from '../../model/Storage';

export const listStorage = async (req, res) => {
    try {
        await getDB();

        res.json({result: await Storage.find({})});

    } catch (error) {
        res.json({error});
    }
}
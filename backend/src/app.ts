import express from 'express';
import cors from 'cors';

import { createProduct } from './routes/Product/createProduct';
import { deleteProduct } from './routes/Product/deleteProduct';
import { findProduct } from './routes/Product/findProduct';

import { addItem } from './routes/Item/addItem';
import { editItem } from './routes/Item/editItem';
import { removeItem } from './routes/Item/removeItem';
import { findItem } from './routes/Item/findItem'

import { createStorage } from './routes/Storage/createStorage';
import { listStorage } from './routes/Storage/listStorage';

const app = express();
const port = process.env.PORT || 3000;

app.use(cors());

app.get('/', (req, res) => {
  res.send('Running!');
});

// Product
app.post('/createProduct', createProduct);
app.delete('/deleteProduct', deleteProduct);
app.get('/findProduct', findProduct);

// item
app.get('/findItem', findItem);
app.post('/addItem', addItem);
app.put('/editItem', editItem);
app.delete('/removeItem', removeItem);

// Storage
app.post('/createStorage', createStorage);
app.get('/listStorage', listStorage);

app.listen(port, () => {
  console.log('App listening on port ', port);
});

module.exports = app;


import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

let db = null;

export const getDB = () => {
  return new Promise((resolve, reject) => {
    if (db) {
      resolve(db);
      return;
    }
    mongoose
      .connect(
        process.env.MONGO_CONNECTION_STRING,
        {
          useNewUrlParser: true, useFindAndModify: false
        }
      )
      .then(response => {
        db = response;
        resolve(response);
      }).catch(err => {
        console.log('Error: ', err);
        reject(err);
      });
  });
};
